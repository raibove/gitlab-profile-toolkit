# Gitlab Profile Toolkit


Welcome to the GitLab Profile Toolkit! This toolkit allows you to easily create and customize your profile README for your GitLab account. With the GitLab Profile Toolkit, you can showcase your skills, interests, and personality in a personalized and engaging way.

## How to use

1. Create a new repository with your username, if its not present already
2. Add the following section to your README.md file, you can give whatever title you want.  Just make sure that you use <!--START_SECTION:random_quote--><!--END_SECTION:random_quote--> in your readme. The workflow will replace this comment with the actual blog post list:

```
# Random quote
<!--START_SECTION:random_quote-->
<!--END_SECTION:random_quote-->

# Recent Blogs
<!--START_SECTION:blog_post-->
<!--END_SECTION:blog_post-->

```
3. Create a .gitlab-ci.yml workflow and in that add 

```
include:
  # include the component located in the current project from the current SHA
  - component: gitlab.com/raibove/components/gitlab-profile-toolkit/random-quote@<version>
    inputs:
        BRANCH_NAME:
            description: "Name of the branch to push the changes to"
            default: "main"                        
        BOT_NAME: 
            description: "Bot's name that appears in the commit log"
            default: "Personal Readme Bot"
        BOT_EMAIL:
            description: "Bot's email, appears in commit message"
            default: "gitlab-runner-bot@example.net"
        COMMIT_MESSAGE: 
            description: "Part of the commit message"
            default: "Update README.md"  
        INCLUDE_RANDOM_QUOTE:
            description: "Include random quote in readme or not"
            default: true
        INCLUDE_BLOG:
            description: "Include hashnode blog in readme"
            default: true
        PUBLICATION_LINK:
            description: "From where should we fetch the blog"
            default: "https://shwetakale.hashnode.dev/"
```
