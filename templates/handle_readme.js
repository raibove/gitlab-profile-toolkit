const fs = require('fs');
const axios = require('axios');

const BASE_URL = 'https://gql.hashnode.com/'
const includeRandomQuote = process.argv[2];
const includeHashnodeBlog = process.argv[3];
const publicationLink = process.argv[4];
const postCount = 10;

const getQuery = (publicationName, limit) => {
    return `{
  publication(host: "${publicationName}") {
    posts(first: ${limit}) {
      totalDocuments
      pageInfo {
        hasNextPage
        endCursor
      }
      edges {
        node {
          url
          title
          brief
          slug
          publishedAt
          coverImage {
            url
          }
          reactionCount
          replyCount
        }
      }
    }
  }
}`
}

const fetchPosts = async (
    publicationName,
    limit
) => {
    const query = getQuery(publicationName, limit)
    const response = await fetch(BASE_URL, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ query })
    })
    const data = await response.json()

    return data
}

const createMarkdownList = (postList) => {
    let listContent = ''

    postList.forEach(post => {
        const { title, url } = post

        listContent += `- [${title}](${url})\n`
    })

    return listContent
}


const getFormattedContent = (
    postList,
    format
) => {
    switch (format) {
        case 'list':
            return createMarkdownList(postList)
        // case 'card':
        //     return createMarkdownCard(postList)
        // case 'stacked':
        //     return createMarkdownStackedLayout(postList)
        // case 'table':
        //     return createMarkdownTable(postList)

        default:
            return createMarkdownList(postList)
    }
}


// Function to fetch a random quote from the API
async function fetchRandomQuote() {
    try {
        const response = await axios.get('https://api.quotable.io/quotes/random');
        return `# Do read a quote before you go \n\n${response.data[0].content}`;
    } catch (error) {
        console.error('Error fetching quote:', error);
        return null;
    }
}

// Function to replace content between comment tags
function updateReadmeContent(data, newContent, tagText) {
    const startTag = `START_SECTION:${tagText}`;
    const endTag = `END_SECTION:${tagText}`;
    // Regular expression to match the content between the comment tags
    const regex = new RegExp(`<!--${startTag}-->([\\s\\S]*?)<!--${endTag}-->`);

    // Check if the comment tags exist
    const match = data.match(regex);
    if (match && match[1]) {
        // Content found between the comment tags
        const oldContent = match[1].trim();

        // Replace the old content with the new content
        const replacedData = data.replace(
            regex,
            `<!--${startTag}-->\n${newContent}\n<!--${endTag}-->`
        );

        return replacedData;
    } else {
        // If the comment tags are not present, log a message
        console.log(`Comment tags ${startTag} and ${endTag} not found. No replacement needed.`);
        return data;
    }
}


// Function to read content from README.md file
async function readFromFile() {
    try {
        const content = fs.readFileSync('README.md', 'utf8');
        return content;
    } catch (error) {
        console.error('Error reading file:', error);
        return null;
    }
}

// Execute the process to fetch a quote and append it to the file
async function main() {
    let readmeContent = await readFromFile();

    if (readmeContent) {
        // Check if the user wants to include weather information and the section is present in README.md
        if (includeHashnodeBlog && readmeContent.includes('<!--START_SECTION:blog_post-->')) {

            const response = await fetchPosts(publicationLink, postCount);
            console.log(response);
            if (response.data.publication) {
                const posts = response.data.publication.posts.edges.map(edge => edge.node)
                const formatedPost = getFormattedContent(posts, 'list')

                if (formatedPost) {
                    readmeContent = await updateReadmeContent(readmeContent, formatedPost, 'blog_post');
                }
            }
        }

        // Update the random quote section
        if (includeRandomQuote && readmeContent.includes('<!--START_SECTION:random_quote-->')) {
            const randomQuote = await fetchRandomQuote();
            if (randomQuote) {
                readmeContent = await updateReadmeContent(readmeContent, randomQuote, 'random_quote');
            }
        }

        // Write the updated content back to README.md
        fs.writeFileSync('README.md', readmeContent, { flag: 'w' });
        console.log('File updated successfully');
    } else {
        console.error('Error reading file');
    }
}

// Start the process
main();